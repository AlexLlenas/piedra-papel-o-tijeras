﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piedra_papel_y_tijeras
{
    class Program
    {
        static void Main(string[] args)
        {

            //loop

            string bucle = "s";
            while (bucle == "s")
            {
                //Entrada
                Console.WriteLine("Eliga entre piedra, papel o tijeras para juegar");
                string respuesta = Console.ReadLine();



                //IA
                var random = new Random();
                string eleccionCom;
                var computadora = random.Next(1, 4);
                switch (computadora)
                {
                    case 1:
                        eleccionCom = "piedra";
                        //Console.WriteLine("Piedra");
                        break;
                    case 2:
                        eleccionCom = "papel";
                        // Console.WriteLine("Papel");
                        break;
                    case 3:
                        eleccionCom = "tijeras";
                        // Console.WriteLine("Tijeras");
                        break;
                }

                // respuesta

                switch (respuesta)
                {
                    case "piedra":
                        {
                            if (computadora == 1)
                                Console.WriteLine($"La computadora eligió piedra, quedaron empate");
                            else if (computadora == 2)
                                Console.WriteLine("La computadora eligió papel, ha perdido");
                            else if (computadora == 3)
                                Console.WriteLine("La computadora eligió tijeras, ha ganado");
                        }
                        break;
                    case "papel":
                        {
                            if (computadora == 1)
                                Console.WriteLine($"La computadora eligió piedra, ha ganado");
                            else if (computadora == 2)
                                Console.WriteLine("La computadora eligió papel, quedaron empate");
                            else if (computadora == 3)
                                Console.WriteLine("La computadora eligió tijeras, ha perdido");
                        }
                        break;
                    case "tijeras":
                        {
                            if (computadora == 1)
                                Console.WriteLine($"La computadora eligió piedra, ha perdido");
                            else if (computadora == 2)
                                Console.WriteLine("La computadora eligió papel, ha ganado");
                            else if (computadora == 3)
                                Console.WriteLine("La computadora eligió tijeras, quedaron empate");
                        }
                        break;
                    default:
                        Console.WriteLine("su respuesta no es válida");
                        break;
                }
                Console.WriteLine();
                Console.WriteLine("Desea volver a jugar?(s/n)");
                bucle = Console.ReadLine();

            }
            
           // Console.ReadKey();
        }
    }
}
